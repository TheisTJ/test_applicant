package com.findwise.cph.greeting;

import com.findwise.cph.greeting.GreetingController;
import com.findwise.cph.greeting.Greeting;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GreetingControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void greeting() {
        // Arrange
        Greeting input = new Greeting();
        input.setContent("Tester");

        HttpEntity<String> request = new HttpEntity<>("Tester");

        // Act
        Greeting output = this.restTemplate.postForObject("/greeting", request, Greeting.class);

        // Assert
        Assert.assertEquals("Hello, Tester!",output.getContent());
    }
}
