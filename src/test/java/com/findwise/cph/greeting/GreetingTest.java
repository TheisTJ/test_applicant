package com.findwise.cph.greeting;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GreetingTest {

    private static Logger logger = LoggerFactory.getLogger(GreetingTest.class);

    @Test
    public void GreetingTest() throws IOException {
        String expected = "{\"id\":0,\"content\":\"Hello Test\"}";
        String testString = "Hello Test";
        Long id = 0L;
        Greeting greeting = new Greeting();
        greeting.setId(id);
        greeting.setContent(testString);

        ObjectMapper objectMapper = new ObjectMapper();
        final String asString = objectMapper.writeValueAsString(greeting);
        logger.info(asString);
        Assert.assertEquals(expected,asString);

        final Greeting greetingFromJsoon = objectMapper.readValue(expected, Greeting.class);
        Assert.assertEquals(greeting,greetingFromJsoon);

    }

}
